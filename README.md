**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

## Utilizacion del Programa

-Para crear una cuenta donde se pueda almacenar Bookmark, ustede debeje ejecutar Principal.py este le mostrara 3 opciones donde usted debera escoger **Registrarse** en este se le pediran unos datos obligatorios.
    [Obligatorios]: Username, Password, Nombres, Email

-Para iniciar sesion donde se pueda almacenar Bookmark, ustede debeje ejecutar Principal.py este le mostrara 3 opciones donde usted debera escoger **iniciar sesion** en este se le pediran unos datos obligatorios.
    [Obligatorios]: Email, Password

-Para cerrar sesion usted debe estar loggeado, le mostrara 12 opciones donde usted debera escoger **Cerrar Sesion** .
    
-Para crear un Booknark usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **Crear un Bookmark** en este se le pediran unos datos obligatorios y unos opcionales que podra dejar en blanco.
    [Obligatorios]: Nombre, URL
    [Opcionales]: Comentario, labels, login, password

-Para editar un Booknark usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **Editar bookmark** en este se le pediran unos datos obligatorios y unos opcionales que podra dejar en blanco, [**Para usar esta opcion usted debe tener un Bookmark creado**].
    [Obligatorios]: Nombre, URL
    [Opcionales]: Comentario, labels, login, password

-Para borrar un Bookmark usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **borrar bookmark** en este se le pediran unos datos obligatorios, [**Para usar esta opcion usted debe tener un Bookmark creado**].
    [Obligatorios]: Nombre

-Para ver un Bookmark usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **ver bookmark** en este se le pediran unos datos obligatorios, [**Para usar esta opcion usted debe tener un Bookmark creado**].
    [Obligatorios]: Nombre

-Para ver todos los Bookmark por usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **ver bookmark relacionados a una cuenta** , [**Para usar esta opcion usted debe tener un Bookmark creado**].
    
-Para ver un Bookmark por label usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **ver por label** en este se le pediran unos datos obligatorios, [**Para usar esta opcion usted debe tener un Bookmark creado y un label**].
    [Obligatorios]: label

-Para borrar un label usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **eliminar labels** en este se le pediran unos datos obligatorios, [**Para usar esta opcion usted debe tener un label creado**].
    [Obligatorios]: label

-Para editar un label usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **editar labels** en este se le pediran unos datos obligatorios, [**Para usar esta opcion usted debe tener un label creado**].
    [Obligatorios]: label

-Para crear un label usted debe estar loggeado, ustede debeje ejecutar Principal.py este le mostrara 12 opciones donde usted debera escoger **creart labels** en este se le pediran unos datos obligatorios
    [Obligatorios]: label