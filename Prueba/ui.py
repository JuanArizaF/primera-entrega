import getpass
import json
from cryptography.fernet import Fernet
from time import gmtime, localtime, strftime


# 0. MENU

def menu():
    print('Bienvenido.........')
    print('1. Registrarse:  ')
    print('2. Iniciar Sesion:  ')
    print('3. Salir:  ')
    opcion = input('Digite su opcion.....')
    return int(opcion)

def menu_post():
    print('1. Cerrar Sesion:  ')
    print('3. Crear bookmark:  ')
    print('4. Ver bookmarks:  ')
    print('5. Salir:  ')
    print('6. Editar bookmark')
    print('7. Borrar bookmarks')
    print('8. Ver bookmark')
    print('9. Ver por labels')                                                                                                                                                                                                                                                                                                                                  
    print('10. Editar labels')
    print('11. Eliminar labels')
    print('12. Ver bookmark relacionados a una cuenta')
    print('13. Crear label')
    opcion = input('Digite su opcion.....')
    return int(opcion)


# 1. CREAR CUENTA // FALTA ENCRIPTACION DEL PASSWORD //

def crear_usuario():
    clave = Fernet.generate_key() 
    f = Fernet(clave)
    valido=False
    while valido ==False:
        username = input('Ingrese su Usuario...')
        if username==("") or username==(" ") or username==("  ") or username[0]==(" "):
            print('No puede dejar el campo vacio ')
        else:
            valido=True  
    valido=False
    while valido ==False:
        passwordin = getpass.getpass()
        if passwordin==("") or passwordin==(" ") or passwordin==("  ") or passwordin[0]==(" "):
            print('No puede dejar el campo vacio ')
        else:
            valido=True  
    password = passwordin.encode('ascii')
    passwordE = f.encrypt(password)
    password1 = str(passwordE)
    valido=False
    while valido ==False:
        nombre = input('Ingrese su Nombre...')
        if nombre==("") or nombre==(" ") or nombre==("  ") or nombre[0]==(" "):
            print('No puede dejar el campo vacio ')
        else:
            valido=True 
    valido=False
    while valido ==False:
        email = input('Ingrese su Email...')
        if email==("") or email==(" ") or email==("  ") or email[0]==(" "):
            print('No puede dejar el campo vacio ')
        else:
            valido=True 
    hora = strftime("%Y-%m-%d- %I:%M:%S", localtime())
    return(username, password1, nombre, email, hora, str(clave))

def guardar_usuario(datos):
    info = {
        'username': datos[0],
        'password': datos[1],
        'nombre':   datos[2],
        'email':    datos[3],
        'hora':     datos[4],
        'clave':    datos[5]
    }
    archivo = open('usuarios.json', 'r')
    memoria = json.load(archivo)
    memoria.append(info)
    archivo.close()
    archivo = open('usuarios.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()


# 2. INICIAR SESIÓN

def iniciar_sesion():
    email = input('Ingrese su Email...')
    password = getpass.getpass()
    archivo = open('usuarios.json', 'r')
    memoria = json.load(archivo)
    #print(len(memoria))
    i=0
    estado=0
    existe =False
    while i < len(memoria):
        #print(memoria[i]['username'])
        if email== (memoria[i]['email']):
            token=(memoria[i]['clave'])
            token = bytes(token[2:-1], 'utf-8')
            contra = bytes((memoria[i]['password'])[2:-1], 'utf-8')
            f = Fernet(token)
            des = f.decrypt(contra)
            final=des.decode('ascii')
            existe=True
            if password== final:
                print('Bienvenido...'+memoria[i]['username'])
                estado=1
            else:
                print('Contraseña Incorrecta')
        i=i+1 
    else:
        if existe==False:
            print('El usuario no existe')
    archivo.close
    return (estado,email)



# 3. CERRAR SESION
def cerrar_sesion():
    estado=0
    return (estado)

# 4. CREAR BOOKMARK
def crear_bookmark(user):
    print('Ingrese los siguientes datos \n')
    nom = True
    while nom == True:
        nombre =input('Nombre del bookmark: ')
        if nombre==(""):
            nom=True
        else:
            nom=False
            print("Nombre Valido")
    #CORRECCION URL
    url = input('URL: ')
    val_url=False
    while val_url==False:
        if url.count(".")<=0  or url[0]==(".") or url[0]==(" "):
            print("El formato del url es incorrecto Ejemplo(Facebook.com)")
            url = input('Ingrese un URL Valido: ')
        else: 
            val_url=True
    if url.count("www.") <=0:
        url=("www.")+url
    if url.count("http://") <=0:
        url=("http://")+url
        

    
    comentario = input('Comentario: ')
    print("Escoge en los Labels Existentes ")
    
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    existe=0
    i=0
    ii=0
    while ii< len(memoria):
        if user==(memoria[ii]['user']):
            print(memoria[ii]['labels'])
        ii=ii+1
    while existe == 0:
        print("Ingrese una 'X' si no quiere ninguno ")
        labels= input('ingrese el nombre label que quieres usar: ')
        if labels != ("X"):
            while i < len(memoria):
                if labels == (memoria[i]['labels']):
                    existe=1
                i=i+1
            if existe==0:
                print("El label no existe")
                i=0
        else:
            print("\n")
            existe=1
            labels=("")
            print("Luego podras cambiarlo o agregar uno")

    archivo.close()
    login = input('Login: ')
    #Encrypcion
    clave = Fernet.generate_key() 
    f = Fernet(clave)
    passwordin = getpass.getpass()
    password = passwordin.encode('ascii')
    passwordE = f.encrypt(password)
    password1 = str(passwordE)
    #print(password1)
    return(nombre, url, comentario, labels, login, password1,user,str(clave))


def guardar_bookmark(datos):
    info = {
        'nombre': datos[0],
        'url': datos[1],
        'comentario':   datos[2],
        'labels':    datos[3],
        'login':     datos[4],
        'password':     datos[5],
        'creador': datos[6],
        'clave': datos[7]
    }
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    memoria.append(info)
    archivo.close()
    archivo = open('bookmark.json', 'w')
    json.dump(memoria, archivo)
    archivo.close


# 5. EDITAR BOOKMARK
def editar_bookmark(user):
    print("BOOKMARK DISPONIBLES")
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i<len(memoria):
        if memoria[i]['creador']==user:
            print(memoria[i]['nombre'])
        i=i+1
    archivo.close()

    nombre =input('Ingresa el nombre del bookmark que quieres editar: ')
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    numero=0
    while i < len(memoria):
        #print(memoria[i]['username'])
        if nombre== (memoria[i]['nombre']): 
            if user==(memoria[i]['creador']):
                print('Lo encontre ')
                numero=i
            else:
                print("/n")
                print('Usted no tiene ningun bookmark con este nombre')
                print("/n")
        i=i+1 
    #DATOS A CAMBIAR
    print('MENU DE CAMBIOS  ')
    print('1.Nombre')
    print('2.Url')
    print('3.Comenatrio')
    print('4.Labels')
    print('5.Login')
    print('6.Password')
    print('7.SALIR')
    salir=0
    while salir==0:
        numero_dato=int(input('Escoja que la opcion o datos quiere cambiar: '))
        if numero_dato==1:
            nom=False
            while nom==False:
                memoria[numero]['nombre']=input('Ingrese el nuevo nombre: ')
                if memoria[numero]['nombre']==(""):
                    print("Tiene que agregar un nombre")
                else:
                    print("Nombre Valido")
                    nom= True
        elif numero_dato==2:
            
            url = input('URL: ')
            val_url=False
            while val_url==False:
                if url.count(".")<=0  or url[0]==(".") or url[0]==(" "):
                    print("El formato del url es incorrecto Ejemplo(Facebook.com)")
                    url = input('Ingrese un URL Valido: ')
                else: 
                    val_url=True
            if url.count("www.") <=0:
                url=("www.")+url
            if url.count("http://") <=0:
                url=("http://")+url

            memoria[numero]['url']=url
        elif numero_dato==3:
            memoria[numero]['comentario']=input('Ingrese el nuevo comentario: ')
        elif numero_dato==4:
            archivo2 = open('labels.json', 'r')
            memoria2 = json.load(archivo2)
            existe=0
            i=0
            ii=0
            while ii< len(memoria2):
                if user==(memoria2[ii]['user']):
                    print(memoria2[ii]['labels'])
                ii=ii+1
            while existe == 0:
                labels= input('Ingrese el nombre label que quieres usar: ')
                while i < len(memoria2):
                    if labels == (memoria2[i]['labels']):
                        memoria[numero]['labels']=labels
                        existe=1
                    i=i+1
                if existe==0:
                    print("El label no existe")
                    i=0

            archivo2.close()
        elif numero_dato==5:
            memoria[numero]['login']=input('Ingrese el nuevo login: ')
        elif numero_dato==6:
           #memoria[numero]['password']=getpass.getpass()
            #Encrypcion
            clave = Fernet.generate_key() 
            f = Fernet(clave)
            passwordin = getpass.getpass()
            password = passwordin.encode('ascii')
            passwordE = f.encrypt(password)
            password1 = str(passwordE)
            #print(password1)
            memoria[numero]['password']=password1
            memoria[numero]['clave']=str(clave)
        elif numero_dato==7:
            salir=1
            print('Cambios guardados')
        
    archivo.close()
    archivo = open('bookmark.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()
                                                                      

# 6. ELIMINAR BOOKMARK
def eliminar_bookmark(user):
    print("\n")
    print("BOOKMARK DISPONIBLES")
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i<len(memoria):
        if memoria[i]['creador']==user:
            print(memoria[i]['nombre'])
        i=i+1
    archivo.close()

    nombre =input('Ingresa el nombre del bookmark que quieres borrar: ')
    archivo = open('bookmark.json', 'r')
    
    i=0
    eliminado=False
    while i < len(memoria):
        #print(memoria[i]['username'])
        if nombre== (memoria[i]['nombre']): 
            if user==(memoria[i]['creador']): 
                print('Lo encontre ')
                numero=i
                del(memoria[numero])
                print("\n")
                print('BOOKMARK ELIMINADO ')
                print("\n")
                eliminado=True
        i=i+1
    if eliminado==False:
        print("\n")
        print('¡¡BOOKMARK NO ENCONTRADO!! ')
        print("\n")

    archivo.close()
    archivo = open('bookmark.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()
    
# 7. VER BOOKMARK
def ver_bookmark(user):
    print('\n')
    print("BOOKMARK DISPONIBLES")
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i<len(memoria):
        if memoria[i]['creador']==user:
            print(memoria[i]['nombre'])
        i=i+1
    archivo.close()
    nombre =input('Ingresa el nombre del bookmark que quieres ver: ')
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    eliminado=False
    while i < len(memoria):
        if nombre== (memoria[i]['nombre']):
            if user==(memoria[i]['creador']):
                print('\n')
                print('Lo encontre ')
                print('NOMBRE: '+memoria[i]['nombre'])
                print('URL: '+memoria[i]['url'])
                print('COMENTARIO: '+memoria[i]['comentario'])
                print('LABELS: '+memoria[i]['labels'])
                print('LOGIN: '+memoria[i]['login'])

                token=(memoria[i]['clave'])
                token = bytes(token[2:-1], 'utf-8')
                contra = bytes((memoria[i]['password'])[2:-1], 'utf-8')
                f = Fernet(token)
                des = f.decrypt(contra)
                final=des.decode('ascii')

                print('PASSWORD: '+final ) 
                print('\n')
                i=len(memoria)
                eliminado=True
        i=i+1
    if eliminado==False:
            print('¡¡BOOKMARK NO ENCONTRADO!! ')

    archivo.close()
    
# 8. VER TODOS LOS BOOKMARKS
def ver_todosbookmark(user):
    print('\n')
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    eliminado=False
    while i < len(memoria):
        #print(memoria[i]['username'])
        if user== (memoria[i]['creador']): 
            print('\n')
            print('Lo encontre ')
            print('NOMBRE: '+memoria[i]['nombre'])
            print('URL: '+memoria[i]['url'])
            print('COMENTARIO: '+memoria[i]['comentario'])
            print('LABELS: '+memoria[i]['labels'])
            print('LOGIN: '+memoria[i]['login'])
            token=(memoria[i]['clave'])
            token = bytes(token[2:-1], 'utf-8')
            contra = bytes((memoria[i]['password'])[2:-1], 'utf-8')
            f = Fernet(token)
            des = f.decrypt(contra)
            final=des.decode('ascii')
            print('PASSWORD: '+final) 
            print('\n')
            eliminado=True
            
        i=i+1
    if eliminado==False:
            print('¡¡BOOKMARK NO ENCONTRADO!! ')

    archivo.close()

# 9. VER LOS BOOKMARKS DE UN LABEL // ETIQUETAS
def ver_labels(user):
    print('\n')
    print("LABELS DISPONIBLES")
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i<len(memoria):
        if memoria[i]['user']==user:
            print(memoria[i]['labels'])
        i=i+1
    archivo.close()
    nombre =input('Ingresa el label de los  bookmark que quieres ver: ')
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    eliminado=False
    while i < len(memoria):
        #print(memoria[i]['username'])
        if nombre== (memoria[i]['labels']): 
            if user==(memoria[i]['creador']):
                print('\n')
                print('Los encontre ')
                print('\n')
                print('NOMBRE: '+memoria[i]['nombre'])
                print('URL: '+memoria[i]['url'])
                print('COMENTARIO: '+memoria[i]['comentario'])
                print('LABELS: '+memoria[i]['labels']) 
                #print('LOGIN: '+memoria[i]['login'])
                #print('PASSWORD: '+memoria[i]['password']) 
                print('\n')
                eliminado=True
        i=i+1
    if eliminado==False:
            print('¡¡BOOKMARK NO ENCONTRADO!! ')

    archivo.close()


# 10. ELIMINAR UN LABEL
def eliminar_label(user):
    print('\n')
    print("LABELS DISPONIBLES")
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i<len(memoria):
        if memoria[i]['user']==user:
            print(memoria[i]['labels'])
        i=i+1
    archivo.close()
    nombre =input('Ingresa el nombre del label que quieres borrar: ')
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    i=0
    print('\n')
    cambio=(' ')
    while i < len(memoria):
        #print(memoria[i]['username'])
        if nombre== (memoria[i]['labels']): 
            print('Lo encontre ')
            numero=i                                                                                       
            memoria[numero]['labels']=cambio
            memoria[numero]['user']=cambio
            del(memoria[numero])
        i=i+1 
    #DATOS A CAMBIAR
    archivo.close()
    archivo = open('labels.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()
    print('\n')
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i < len(memoria):
        if nombre== (memoria[i]['labels']): 
            print('LABELS: '+memoria[i]['labels']) 
            memoria[i]['labels']=("")
            print("\n")
        i=i+1
    archivo.close()
    archivo = open('bookmark.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()


# 11. EDITAR UN LABEL
def editar_label(user):
    print('\n')
    print("LABELS DISPONIBLES")
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i<len(memoria):
        if memoria[i]['user']==user:
            print(memoria[i]['labels'])
        i=i+1
    archivo.close()
    nombre =input('Ingresa el nombre del label que quieres editar: ')
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    i=0
    print('\n')
    cambio=input('Ingrese el nuevo nombre: ')
    while i < len(memoria):
        if nombre== (memoria[i]['labels']): 
            print('Lo encontre ')
            numero=i
            memoria[numero]['labels']=cambio
        i=i+1 
    #DATOS A CAMBIAR
    archivo.close()
    archivo = open('labels.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()
    print('\n')
    archivo = open('bookmark.json', 'r')
    memoria = json.load(archivo)
    i=0
    while i < len(memoria):
        #print(memoria[i]['username'])
        if nombre== (memoria[i]['labels']): 
            print('LABELS: '+memoria[i]['labels']) 
            memoria[i]['labels']=cambio
            print("\n")
        i=i+1
    archivo.close()
    archivo = open('bookmark.json', 'w')
    json.dump(memoria, archivo)
    archivo.close()


# 12. CREAR UN LABEL
def crear_label(user):
    archivo = open('labels.json', 'r')
    memoria = json.load(archivo)
    print("/n")
    label=input('Ingrese el nombre del label :')
    print("/n")
    info={
        'labels':label,
        'user':user
    }
    memoria.append(info)
    archivo.close()
    archivo = open('labels.json', 'w')
    json.dump(memoria, archivo)
    archivo.close

